vpc_cidr_block = "192.168.0.0/16"
subnet_cidr_blocks = "192.168.10.0/24"
availability_zone = "ap-south-1a"
all_ipv4 = "0.0.0.0/0"
env_prefix = "java-maven-terra"
instance_type = "t2.micro"
region = "ap-south-1"

