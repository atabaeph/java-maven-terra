variable "vpc_cidr_block" {}
variable "subnet_cidr_blocks" {}
variable "availability_zone" {}
variable "env_prefix" {}
variable "all_ipv4" {}
variable "instance_type" {}
variable "region" {}