provider "aws" {    
    region     = var.region 
}




################# NETWORK ###################

resource "aws_vpc" "my_vpc" {
    cidr_block = var.vpc_cidr_block

    tags = {
      "Name" = "${var.env_prefix}-vpc"
    }
}

resource "aws_subnet" "my_subnet" {
    vpc_id = aws_vpc.my_vpc.id
    cidr_block = var.subnet_cidr_blocks
    availability_zone = var.availability_zone

    tags = {
      "Name" = "${var.env_prefix}-subnet-1"
    }
}

resource "aws_internet_gateway" "my_igw" {
    vpc_id = aws_vpc.my_vpc.id

    tags = {
        "Name" = "${var.env_prefix}-igw"
    }
}

resource "aws_route_table" "my_routetable" {
    vpc_id = aws_vpc.my_vpc.id

    route {
        cidr_block = var.all_ipv4
        gateway_id = aws_internet_gateway.my_igw.id
    }

    tags = {
      "Name" = "${var.env_prefix}-routetable"
    }
}

resource "aws_route_table_association" "my_routetable_astn" {
    subnet_id = aws_subnet.my_subnet.id
    route_table_id = aws_route_table.my_routetable.id
}


resource "aws_security_group" "my_sg" {
    name = "my_sg"
    vpc_id = aws_vpc.my_vpc.id

    ingress {        
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [var.all_ipv4]
    }

    ingress {        
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = [var.all_ipv4]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = [var.all_ipv4]
    }

    tags = {
      "Name" = "${var.env_prefix}-sg"
    }

}



############# COMPUTE ##############

data "aws_ami" "my_latest_amazonlinux" {
    owners = ["amazon"]
    most_recent = true
    filter {
        name  = "name" #what we are filtering
        values = ["amzn2-ami-hvm-*-x86_64-gp2"]
    }
}

resource "aws_instance" "my_ec2" { 
    ami = data.aws_ami.my_latest_amazonlinux.id
    instance_type = var.instance_type
    subnet_id = aws_subnet.my_subnet.id
    vpc_security_group_ids = [aws_security_group.my_sg.id]
    availability_zone = var.availability_zone
    associate_public_ip_address = true
    key_name = "Mumbai"    
    user_data = file("bootstrap.sh")    

    tags = {
      "Name" = "${var.env_prefix}-server"
    }
}

output "aws_instance_public_ip" {
    value = aws_instance.my_ec2.public_ip
}
