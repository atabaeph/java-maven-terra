FROM openjdk:8u212-jre-alpine3.9
WORKDIR /usr/app 
COPY ./target/java-maven-terra-*.jar .
CMD java -jar java-maven-terra-*.jar