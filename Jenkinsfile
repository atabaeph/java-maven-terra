pipeline {
    agent any

    tools {
        maven 'my-maven'
    }

    environment {
        REMOTE_SERVER_USER = 'ec2-user'
        //REMOTE_SERVER = ''
    }

    stages {
        stage("STEP 1: Increment app version") {
            steps{
                script {
                    echo "incrementing app version..."
                    sh 'mvn build-helper:parse-version versions:set \
                        -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
                        versions:commit'
                    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'                     
                    env.IMAGE_NAME = matcher[0][1]               
                }
            }
        }        

        stage("STEP 2: Building app") {
            steps{
                script {
                    echo "building app...!"
                    sh 'mvn clean package'
                }
            }            
        }

        stage("STEP 3: Build docker image and push to dockerhub") {
            steps {
                script {
                echo "building docker image..."
                /*withCredentials([usernamePassword(credentialsId: 'dockerhub-cred', 
                                                  passwordVariable: 'PASSWORD_VAR', 
                                                  usernameVariable: 'USERNAME_VAR')]) {
                    sh "docker build -t abubandit/java-maven-terra:${IMAGE_NAME} ."
                    sh "echo $PASSWORD_VAR | docker login -u $USERNAME_VAR --password-stdin"
                    sh "docker push abubandit/java-maven-terra:${IMAGE_NAME}"
                   }     */
                }
            }
        }

        stage("STEP 4: Provision infra with Terraform") {
            environment {
                AWS_ACCESS_KEY_ID = credentials('aws-access-key-id')
                AWS_SECRET_ACCESS_KEY = credentials('aws-secret-access-key')
            }
            steps {
                script {
                    dir('terraform') { //go to terraform dir
                        sh "terraform init"
                        sh "terraform apply --auto-approve"
                        EC2_PUBLIC_IP = sh(
                            script: "terraform output aws_instance_public_ip",
                            returnStdout: true
                        ).trim()
                    }
                }
            }
        }
       
        stage('STEP 5: Deploy app to remote server WITH docker-compose') {
            steps {
                script {        
                    echo "waiting for EC2 starting"
                    sleep(time: 90, unit: "SECONDS")
                    echo "IP HAS ASSIGNED: ${EC2_PUBLIC_IP}"

                    def deployScript = "bash ./deployScript.sh ${IMAGE_NAME}"
                    def ec2_instance = "ec2-user@$EC2_PUBLIC_IP"

                    sshagent(['ec2-cred']) {         
                        sh "scp -o StrictHostKeyChecking=no deployScript.sh $ec2_instance:/home/ec2-user"                                       
                        sh "scp -o StrictHostKeyChecking=no docker-compose.yml $ec2_instance:/home/ec2-user"
                        sh "ssh -o StrictHostKeyChecking=no $ec2_instance ${deployScript}"
                    }
                }                
            }
        }

        stage('STEP 6: Update version number in git pom.xml file'){
            steps {
                script {
                    echo "update version"
                    /*
                    withCredentials([usernamePassword(credentialsId: 'gitlab-cred', passwordVariable: 'PASSWORD_VAR', usernameVariable: 'USERNAME_VAR')]) {
                        sh "git remote set-url origin https://${USERNAME_VAR}:${PASSWORD_VAR}@gitlab.com/atabaeph/java-maven-terra.git"
                        sh 'git checkout -B main'
                        sh 'git add .'
                        sh 'git commit -m "update app version from jenkins in pom.xml"'
                        sh 'git push origin main'                
                    }
                    */
                }
            }
        }
    }
}